//
//  UserLogoTableViewCell.swift
//  Midlands Rider
//
//  Created by Mauro Bazan on 12/12/2018.
//  Copyright © 2018 Chris Thomson. All rights reserved.
//

import UIKit

class UserLogoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userDisplayName: UILabel!
 
    @IBOutlet weak var userLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userLogo.layer.borderWidth = 1
        userLogo.layer.masksToBounds = false
        userLogo.layer.borderColor = UIColor.black.cgColor
        userLogo.layer.cornerRadius = userLogo.frame.height/2
        userLogo.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
