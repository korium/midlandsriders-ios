//
//  MenuItemViewModel.swift
//  Midlands Rider
//
//  Created by Mauro Bazan on 12/12/2018.
//  Copyright © 2018 Chris Thomson. All rights reserved.
//

import UIKit

class MenuItemViewModel: NSObject {
    
    let icon: UIImage
    let displayName: String
    
    init!(icon: UIImage, name: String) {
        self.icon = icon
        self.displayName = name
    }

}
