//
//  ContentViewController.swift
//  Example
//
//  Created by Teodor Patras on 16/06/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit
import SideMenuController

class ContentViewController: UIViewController, SideMenuControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
    }
    
    @IBAction func presentAction() {
        present(ViewController.fromStoryboard, animated: true, completion: nil)
    }
    
    
    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
        
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
       
    }
}

