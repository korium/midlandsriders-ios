//
//  MenuController.swift
//  Example
//
//  Created by Teodor Patras on 16/06/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {
    
    let segues = ["showHomeController", "showEventController", "showCalendarController", "showShopController", "showAboutController"]
    private var previousIndex: NSIndexPath?
    
    var menu = [MenuItemViewModel]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMenuData();
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count + 1
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
     
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuUserLogo") as!
                UserLogoTableViewCell
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuItemTableViewCell
            
            cell.icon.image = menu[indexPath.row - 1].icon
            cell.menuName.text = menu[indexPath.row - 1].displayName
            
            return cell
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 280
        }
        return 60
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath)  {
        
        if let index = previousIndex {
            tableView.deselectRow(at: index as IndexPath, animated: true)
        }
        if indexPath.row != 0 {
            sideMenuController?.performSegue(withIdentifier: segues[indexPath.row  - 1] , sender: nil)
            previousIndex = indexPath as NSIndexPath?
        }
      
    }
    
    private func loadMenuData(){
        
        let homeIcon = UIImage(named: "home-icon")!
        let eventIcon = UIImage(named: "event-icon")!
        let calendarIcon = UIImage(named: "calendar-icon")!
        let shopIcon = UIImage(named: "shop-icon")!
        let aboutIcon = UIImage(named: "about-icon")!
       
        guard let homeMenu = MenuItemViewModel(icon: homeIcon,name: "Home") else {
            fatalError("Unable to gather home icon")
        }
          guard let eventMenu = MenuItemViewModel(icon: eventIcon,name: "Event") else {
            fatalError("Unable to gather event icon")
        }
          guard let calendarMenu = MenuItemViewModel(icon: calendarIcon,name: "Calendar") else {
            fatalError("Unable to gather calendar icon")
        }
        guard let shopMenu = MenuItemViewModel(icon: shopIcon,name: "Shop") else {
            fatalError("Unable to gather shop icon")
        }
        guard let aboutMenu = MenuItemViewModel(icon: aboutIcon,name: "About us") else {
            fatalError("Unable to gather about icon")
        }
        
        
        menu += [homeMenu, eventMenu, calendarMenu, shopMenu, aboutMenu]
        
        
        
    }
    
}
