//
//  MenuItemTableViewCell.swift
//  Midlands Rider
//
//  Created by Mauro Bazan on 12/12/2018.
//  Copyright © 2018 Chris Thomson. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {


    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
